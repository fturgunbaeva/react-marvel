export const ADD = "ADD";
function allListAction(payload) {
  return {
    type: ADD,
    payload
  }
}
export function addHerosList() {
  return dispatch => {
    const api = require('marvel-comics-api')
    api('characters', {
      publicKey: '279fb09e4a31cf5f8d59f8183e8c63ab',
      privateKey: 'ad7e46758dae450945cd6972fbc19b0c3af25c0e',
      timeout: 2000,
      query: {
        limit: 28
      }
    }, function (err, body) {
      if (err) throw err
      dispatch(allListAction(body.data.results));
    })
  }
}

