import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

//style
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './index.css'

import {BrowserRouter, Switch, Route} from 'react-router-dom'
import './assets/css/style.css';
import {Provider} from 'react-redux'
import {Home} from "./pages/Home";
import {Login} from "./pages/Login";
import Comics from "./pages/Comics";
import {Events} from "./pages/Events";
import {Heros} from "./pages/Heros";
import {Stories} from "./pages/Stories";
import store from "./store/index";


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path={'/'} component={Home}/>
        <Route exact path={'/login'} component={Login}/>
        <Route exact path={'/comics'} component={Comics}/>
        <Route exact path={'/events'} component={Events}/>
        <Route exact path={'/heros'} component={Heros}/>
        <Route exact path={'/stories'} component={Stories}/>
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

