import React, {useCallback, useEffect} from "react";
import {useSelector, useDispatch, connect} from "react-redux";
import {Header} from "../components/partials/Header";
import {HeadTitle} from "../components/HeadTitle";
import {ComicsItem} from "../components/Cards/ComicsItem";
import {
  addComicsList,
  searchItem, toggleItem,
} from "../actions/comics.action";


function Comics(props) {
  const dataList = useSelector(state => state.comics);
  const dispatch = useDispatch();

  const searchComics = useCallback(e => {
    const {target} = e;
    dispatch(searchItem(target.value));
  }, [dispatch]);
  const toggleClass = useCallback((id) => {
    dispatch(toggleItem(id));
  });

  useEffect(() => {
    dispatch(addComicsList());
  },   [dispatch]);


  return (
    <div>
      <Header/>
      <HeadTitle title='Comics' search={searchComics} />
      <div className="container">
        <div className="grid-comics">
          {dataList.data.map((item, index) => {
            return (<ComicsItem key={index} item={item} toggle={toggleClass}/>);
          })}
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Comics);
