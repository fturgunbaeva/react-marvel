import React, {useEffect} from "react";
import {Header} from "../components/partials/Header";
import {HeadTitle} from "../components/HeadTitle";
import {HerosItem} from "../components/Cards/HerosItems";
import {useDispatch, useSelector} from "react-redux";
import {addHerosList} from "../actions/heros.action";


export const Heros = () => {
  const dataList = useSelector(state => state.heros);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addHerosList());
  }, []);

  return (
    <div>
      <Header/>
      <HeadTitle title='Heros'/>
      <div className="container">
        <div className="grid-comics">
          {dataList.data.map(item => {
            return (
                  <HerosItem item={item}/>
            );
          })}
        </div>
      </div>
    </div>
  )
}
