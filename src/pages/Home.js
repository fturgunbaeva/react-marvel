import React from 'react';
import {Header} from "../components/partials/Header";
import {Banner} from "../components/Banner";


export const Home = () => {
  return (
    <div>
      <Header/>
      <Banner/>
    </div>
  )
}
