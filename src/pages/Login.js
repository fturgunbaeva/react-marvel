import React from 'react'
import {MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBInput} from "mdbreact";
import {usersData} from '../data/users'

export const Login = () => {

  function  loginSubmit(e) {
    e.preventDefault()
    if (usersData.username === e.target.username.value && usersData.password === e.target.password.value) {
      console.log('success');

    } else {
      console.log('error')
    }
  }
  return (
    <div className='login-page'>
      <MDBCol md="4">
        <MDBCard>
          <MDBCardBody>
            <form onSubmit={loginSubmit}>
              <div className="white-text">
                <MDBInput label="Username" icon="envelope" type="text" name='username' className='white-color' required/>
                <MDBInput label="Password" icon="lock" type="password" name='password'  className='white-color' required/>
              </div>
              <div className="text-center">
                <MDBBtn gradient="blue" type="submit">Login</MDBBtn>
              </div>
            </form>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </div>
  )
}
