import React, {useEffect} from "react";
import {Header} from "../components/partials/Header";
import {HeadTitle} from "../components/HeadTitle";
import {StoriesItem} from "../components/Cards/StoriesItem";
import {useDispatch, useSelector} from "react-redux";
import {addStoriesList} from "../actions/stories.action";

export const Stories = () => {
  const dataList = useSelector(state => state.stories);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addStoriesList());
  }, []);
  return (
    <div>
      <Header/>
      <HeadTitle title='Stories'/>
      <div className="container">
        <div className="stories-grid">
          {dataList.data.map(item => {
            return (
              <StoriesItem item={item}/>
            );
          })}
        </div>
      </div>

    </div>
  )
}
