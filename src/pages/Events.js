import React, {useEffect, useCallback} from "react";
import {Header} from "../components/partials/Header";
import {HeadTitle} from "../components/HeadTitle";
import {EventsItem} from "../components/Cards/EventsItem";
import {useDispatch, useSelector} from "react-redux";
import {addEventsList, searchItem} from "../actions/events.action";

export const Events = () => {
  const dataList = useSelector(state => state.events);
  const dispatch = useDispatch();

  const searchEvents = useCallback(e => {
    const { target } = e;
    dispatch(searchItem(target.value));
  });

  useEffect(() => {
    dispatch(addEventsList());
  }, []);



  return (
    <div>
      <Header/>
      <HeadTitle title='Events' search={searchEvents} />
      <div className="container">
        <div className="grid-events">
          {dataList.data.map(item => {
            return (
              <EventsItem item={item}/>
            );
          })}
        </div>
      </div>
    </div>
  )
}
