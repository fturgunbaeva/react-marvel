import { ADD } from '../actions/stories.action';

const initialState = {
  data: []
};

function StoriesReducer(state = initialState, action) {
  switch (action.type) {
    case ADD:
      const data = action.payload.map((item, index) => {
        return {...item, ...{
            id: index,
          }
        };
      });
      const newState = { ...state, data: data };
      return newState;
    default:
      return state;
  }
}

export default StoriesReducer;
