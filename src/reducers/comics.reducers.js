import { ADD, SEARCH, TOGGLE } from '../actions/comics.action';

const initialState = {
  data: []
};

function ComicsReducer(state = initialState, action) {
  const newData = state.data.slice();
  switch (action.type) {
    case ADD:
      const data = action.payload.map((item, index) => {
        return {
            ...item, ...{
            id: index,
            isVisible: true,
            isToggled: false
          }
        };
      });
      return { ...state, data: data };
    case SEARCH:
      const datas = newData.map(item => {
        if (item.title.includes(action.payload)) {
          return { ...item, ...{ isVisible: true } };
        } else {
          return { ...item, ...{ isVisible: false } };
        }
      });
      return { ...state, datas };

    case TOGGLE:
      console.log('Payload: ', action.payload);
      console.log('DATA: ', newData);
      const dataToggle = newData.map(item => {
        if (item.id === action.payload) {
          return {
            ...item,
            ...{isToggled: !item.isToggled}
          }
        } else {
          return item;
        }
      });
      return {...state, dataToggle};


    default:
      return state;
  }
}

export default ComicsReducer;
