import {combineReducers} from 'redux';

import ComicsReducer from './comics.reducers'
import HerosReducer from './heros.reducer'
import StoriesReducer from "./stories.reducer";
import EventsReducer from "./events.reducer";

const reducer = combineReducers({
  comics: ComicsReducer,
  heros: HerosReducer,
  stories: StoriesReducer,
  events: EventsReducer
})

export default reducer
