import { ADD, SEARCH } from '../actions/events.action';

const initialState = {
  data: []
};

function EventsReducer(state = initialState, action) {
  const newData = state.data.slice();
  switch (action.type) {
    case ADD:
      const data = action.payload.map((item, index) => {
        return {...item, ...{
            id: index,
            isVisible: true,
          }
        };
      });
      return  { ...state, data: data };
    case SEARCH:
      const datas = newData.map(item => {
        if (item.title.includes(action.payload)) {
          return { ...item, ...{ isVisible: true } };
        } else {
          return { ...item, ...{ isVisible: false } };
        }
      });
      return { ...state, datas };
    default:
      return state;
  }
}

export default EventsReducer;
