import React, {memo} from 'react';
import {MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText} from 'mdbreact';

export const HerosItem = memo(({item}) => {
  const imgUrl = item.thumbnail.path + '/portrait_xlarge.' + item.thumbnail.extension
  return  <MDBCard>
    <MDBCardImage className="img-fluid" src={imgUrl}/>
    <MDBCardBody>
      <MDBCardTitle>{item.name}</MDBCardTitle>
      <MDBCardText>
      </MDBCardText>
    </MDBCardBody>
  </MDBCard>

});
