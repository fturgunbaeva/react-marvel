import React, {memo} from 'react';
import {MDBCard, MDBCardBody, MDBCardTitle, MDBCardText} from 'mdbreact';

export const StoriesItem = memo(({item}) => {
  return  <MDBCard>
    <MDBCardBody>
      <MDBCardTitle>{item.title }</MDBCardTitle>
      <MDBCardText>
        <span>Description : {item.description }</span>
        <span>Modified  : {item.modified  }</span>
      </MDBCardText>
    </MDBCardBody>
  </MDBCard>

});
