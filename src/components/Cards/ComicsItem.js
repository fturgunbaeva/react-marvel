import React from 'react';
import classNamePlugin from 'classnames';
import {MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBCardTitle} from 'mdbreact';

export const ComicsItem = ({item, toggle}) => {
  const imgUrl = item.thumbnail.path + '/portrait_xlarge.' + item.thumbnail.extension
  // if (!item.thumbnail || !item.thumbnail.path ){
  //   return (const notImgUrl = 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/4c002e0305708')
  // }

  return (
    <MDBCard>
      <MDBCardImage className="img-fluid" src={imgUrl}/>
      <MDBCardBody>
        <MDBCardTitle>{item.title}</MDBCardTitle>
        <MDBCardText>
          <span>ID: {item.id}</span>
          <span>pageCount: {item.pageCount}</span>
          <span>Modified: {item.modified}</span>
        </MDBCardText>
        <div
          className={classNamePlugin('like-btn', {'toggled': item.isToggled})}
          onClick={() => toggle(item.id)}>
        </div>
      </MDBCardBody>
    </MDBCard>
  )
};

