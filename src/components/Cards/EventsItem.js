import React, {memo} from 'react';
import {MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText} from 'mdbreact';

export const EventsItem = memo(({item}) => {
  const imgUrl = item.thumbnail.path + '/portrait_xlarge.' + item.thumbnail.extension
  return <MDBCard>
    <MDBCardImage className="img-fluid"  src={imgUrl} />
    <MDBCardBody>
      <MDBCardTitle>{item.title}</MDBCardTitle>
      <MDBCardText>
        <span>ID: {item.id}</span>
        <span>Description: {item.description}</span>
        <span>Modified: {item.modified}</span>
        <span>Start: {item.start}</span>
        <span>End: {item.end}</span>
      </MDBCardText>
    </MDBCardBody>
  </MDBCard>

});
