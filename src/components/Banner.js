import React from "react";
import banner from '../assets/img/banner.jpg'
export const Banner = () => {
  return (
    <div className='banner'>
      <img src={banner} alt=""/>
      <div className="text">
        <h1>This is React Marvel App</h1>
        <p>just for fun</p>
      </div>
    </div>
  )
}
