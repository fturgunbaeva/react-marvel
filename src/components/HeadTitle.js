import React from "react";
import {Search} from "./Search";

export const HeadTitle = (({title, search}) => {
  return (
    <div className='container'>
      <div className="inner-page">
        <div className="title">
          <h1>{title}</h1>
          <Search search={search} />
        </div>
      </div>
    </div>
  )
})
