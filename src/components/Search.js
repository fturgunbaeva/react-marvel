import React from "react";
import {MDBCol} from "mdbreact";

export const Search = ({search}) => {
  return (
    <MDBCol md="3">
      <div className="form-group m-0">
        <input
          onChange={search}
          type="text"
          className="form-control"
          id="formGroupExampleInput" placeholder='Search'
        />
      </div>
    </MDBCol>
  )
}
