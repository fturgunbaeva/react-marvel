import React from "react";
import {NavLink} from 'react-router-dom';

import disney from '../../assets/img/disney.png'

export const Header = () => {
  return (
      <div>
        <header>
          <div className="center">
            <div className="logo"> </div>
            <a href="https://www.disneyplus.com/">
              <img src={disney} alt=""/>
            </a>
          </div>
          <div className="container">
            <ul>
              <li>
                <NavLink to={"/"} exact>Home</NavLink>
              </li>
              <li>
                <NavLink to={"/comics"} exact>Comics</NavLink>
              </li>
              <li>
                <NavLink to={"/heros"} exact>Heros</NavLink>
              </li>
              <li>
                <NavLink to={"/stories"} exact>Stories</NavLink>
              </li>
              <li>
                <NavLink to={"/events"} exact>Events</NavLink>
              </li>
            </ul>
          </div>
        </header>
      </div>

  )
}
